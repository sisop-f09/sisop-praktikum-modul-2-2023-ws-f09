# Soal 1
Pada program ini terdapat beberapa fungsi yaitu `downloadFile()`, `unzipper`, `getFile`, `newDirectory`, `hewanPindah`, dan `zipper`

### `downloadFile()`
``` c
void downloadFile(char *url, char *namefile)
{
  int stat; /* untuk mendeklarasikan variabel stat yang bertipe data int*/
  pid_t IDdownload = fork(); /* untuk mendeklarasikan variabel stat yang bertipe data var*/

  if (IDdownload == 0) //melakukan  kondisi pengececekan, apabila 0 berarti proses child//
  {
    char *args[] = {"wget", "--no-check-certificate", url, "-q", "-O", namefile, NULL};
    execv("/usr/bin/wget", args); //untuk menjalankan program wget dengan argumen ags//
  }

  // fungsi untuk menunggu proses child selesai lanjut ke proses parent
  waitpid(IDdownload, &stat, 0); 
}
```
#### Penjelasan
Function `downloadFile` yang memiliki dua parameter yaitu url dan namefile, yang berfungsi untuk melakukan download file dari internet menggunakan program wget pada sistem operasi Linux.

1. Function downloadFile() diawali dengan deklarasi variabel stat yang bertipe data integer dan IDdownload yang bertipe data pid_t (process ID). 
2. Kemudian, dilakukan pemanggilan sistem fork() yang menghasilkan dua proses yang berbeda, yaitu parent process dan child process. 
3. Proses child process yang dihasilkan oleh fork() kemudian akan menjalankan program wget untuk mendownload file dari url yang ditentukan dan menyimpan file tersebut dengan nama file yang ditentukan juga.

Program wget dijalankan dengan menggunakan execv() dan diberikan argumen-argumen yang disimpan dalam array args. Argumen-argumen tersebut adalah:

`wget` : merupakan nama program wget
`--no-check-certificate` : untuk mengabaikan sertifikat SSL/TLS yang tidak valid atau tidak terpercaya
`url` : alamat URL dari file yang akan didownload
`-q` : untuk menjalankan program wget secara diam-diam (tanpa output yang ditampilkan ke layar)
`-O` : untuk menentukan nama file hasil download
`namefile` : nama file hasil download yang diberikan sebagai argumen pada function downloadFile()
`NULL` : sebagai penanda akhir dari array args

Setelah program wget selesai dijalankan, maka child process akan berakhir dan parent process akan menunggu proses child selesai menggunakan fungsi waitpid(). Setelah proses child selesai, maka parent process akan melanjutkan eksekusi programnya.

### `unzipper()`
```c
void unzipper(char *sourceDir)
{
  pid_t IDunzip = fork(); /** mendeklarasikan var unzipId bertipe data pid_t 
  dengan init variabel tersebut dengan fungsi fork*/

  if (IDunzip == 0) // melakukan kondisi pengecekan, apabila 0 maka proses child*/
  {
    char *args[] = {"unzip", "-q", sourceDir, "-d", ".", NULL};
    execv("/usr/bin/unzip", args);
  }

  waitpid(IDunzip, NULL, 0); //untuk menunggu proses child selesai//
}
```
#### Penjelasan
Function `unzipper`   memiliki satu parameter yaitu sourceDir, yang berfungsi untuk melakukan ekstraksi (unzip) file pada direktori tertentu menggunakan program unzip pada sistem operasi Linux.

1. Function unzipper() diawali dengan deklarasi variabel IDunzip yang bertipe data pid_t (process ID). 
2. Kemudian, dilakukan pemanggilan sistem fork() yang menghasilkan dua proses yang berbeda, yaitu parent process dan child process. 
3. Proses child process yang dihasilkan oleh fork() kemudian akan menjalankan program unzip untuk mengekstrak file pada direktori tertentu.

Program unzip dijalankan dengan menggunakan execv() dan diberikan argumen-argumen yang disimpan dalam array args. Argumen-argumen tersebut adalah:

`unzip` : merupakan nama program unzip
`-q` : untuk menjalankan program unzip secara diam-diam (tanpa output yang ditampilkan ke layar)
`sourceDir` : direktori tempat file yang akan diekstrak
`-d` : untuk menentukan direktori tujuan ekstraksi
`.` : direktori tujuan ekstraksi diatur ke direktori saat ini (current directory)
`NULL` : sebagai penanda akhir dari array args
Setelah program unzip selesai dijalankan, maka child process akan berakhir dan parent process akan menunggu proses child selesai menggunakan fungsi waitpid(). Setelah proses child selesai, maka parent process akan melanjutkan eksekusi programnya.




### `getFile()`
``` c
void getFile(char *directory)
{
  int stat;
  char *path[100];

  struct dirent *dp;
  DIR *folder;
  srand(time(NULL));
  folder = opendir(directory);

  if (folder != NULL)
  {
    int i = 0;
    while ((dp = readdir(folder)) != NULL)
    {
      if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".jpg") != NULL)
      {
        if (dp->d_type == DT_REG)
        {
          char *token = dp->d_name;
          path[i] = token;
          i++;
        }
      }
    }
    int size = sizeof(*path);
    int random = rand() % size;

    // Create file txt
    printf("Hewan Yang Dijaga : %s \n", strtok(path[random], "."));
    FILE *file;
    file = fopen("penjaga.txt", "w");
    fprintf(file, "Hewan Yang Dijaga : %s", strtok(path[random], "."));
    fclose(file);
    closedir(folder);
  }
}
```
##### Penjelasan
Function `getFile` yang memiliki satu parameter yaitu directory, yang berfungsi untuk mendapatkan file gambar (.jpg) dari direktori tertentu secara acak dan menuliskan nama file tersebut ke dalam file teks baru bernama "penjaga.txt".

1. Function getFile() dimulai dengan mendeklarasikan variabel stat yang bertipe data int dan variabel path yang merupakan array of pointer bertipe data char sebanyak 100 elemen. 
2. Kemudian, dilakukan deklarasi struct dirent dan DIR untuk membaca isi dari direktori yang diinputkan.
3. Selanjutnya, dilakukan pengacakan nama file dengan menggunakan fungsi rand() yang diinisialisasi dengan time(NULL) untuk men-generate nilai random yang berbeda setiap kali program dijalankan. Kemudian, dilakukan perulangan untuk membaca setiap file pada direktori tersebut dan menyimpan nama file dengan ekstensi ".jpg" ke dalam array path.
4. Setelah itu, program memilih nama file secara acak dari array path dan menuliskan nama file tersebut ke dalam file teks baru bernama "penjaga.txt" dengan menggunakan fungsi fprintf(). Setelah penulisan file teks selesai, maka program akan menutup file dengan fungsi fclose() dan menutup direktori dengan fungsi closedir().

Secara keseluruhan, fungsi getFile() berguna untuk mendapatkan file gambar secara acak dari suatu direktori dan menuliskan nama file tersebut ke dalam file teks baru.

### `newDirectory()`
``` c
void newDirectory()
{
  int stat;
  id_t child_id;

  if ((child_id = fork()) == 0) /* melakukan proses pengecekan, apabila 0 maka proses child*/
  {
    char *args[] = {"mkdir", "-p", "HewanDarat", NULL};
    execv("/bin/mkdir", args); 
  }
  while ((wait(&stat)) > 0) /* menunggu proses child selesai*/
    ;

  if ((child_id = fork()) == 0)
  {
    char *args[] = {"mkdir", "-p", "HewanAir", NULL};
    execv("/bin/mkdir", args); /* menjalankan program mkdir dengan argumen args 
    sebanyak 3 kali*/
  }
  while ((wait(&stat)) > 0)
    ;
  if ((child_id = fork()) == 0)
  {
    char *args[] = {"mkdir", "-p", "HewanAmphibi", NULL};
    execv("/bin/mkdir", args);
  }
  while ((wait(&stat)) > 0)
    ;

  waitpid(child_id, &stat, 0); /* untuk menunggu proses child selesai*/
}
```
#### Penjelasan
Function `newDirectory()` yang berfungsi untuk membuat tiga direktori baru dengan nama "HewanDarat", "HewanAir", dan "HewanAmphibi" menggunakan perintah mkdir.

1. Proses pembuatan direktori dilakukan menggunakan fungsi fork() untuk membuat proses child yang kemudian menjalankan perintah mkdir dengan menggunakan execv(). 
2. Proses parent kemudian menunggu proses child selesai dengan menggunakan perintah wait() atau waitpid().
3. Dalam kode tersebut, tiga kali dilakukan pemanggilan fungsi fork() untuk menjalankan perintah mkdir yang berbeda untuk setiap nama direktori yang berbeda. Setelah itu dilakukan pengecekan dengan fungsi wait() atau waitpid() untuk menunggu proses child selesai sebelum kembali ke proses parent.

### `hewanPindah()`
```c
void HewanPindah(char *soruce, char *des1, char *des2, char *des3)
{
  int stat;
  id_t child_id;
  struct dirent dp; /*untuk mendeklarasikan variabel dp dgn tipe data struct dirent*/
  DIR folder; /*mendeklarasi variabel folder yang bertipe data DIR*/
  folder = opendir(soruce); /* membuka direktori yang dipindahkan*/

  if (folder != NULL) //melakukan pengecekan apakah ada atau tidak//
  {
    while ((dp = readdir(folder)) != NULL) /*perulangan selama direk yang dipindahkan 
    masih ada*/
    {
      if (dp -> d_type == DT_REG) /*melakukan pengecekan apakah file yang dipindahkan 
      file biasa atau bukan*/
      {
        // jika cocok dengan darat
        if (strstr(dp -> d_name, "darat") != NULL)
        { //pengecekan apakah 0 atau tidak, jika 0 maka proses child//
          if ((child_id = fork()) == 0)
          { // deklarasi var argv tipe data char dengan init argumen yang digunakan untuk 
          // menjalankan program mv//
            char *argv[] = {"mv", dp -> d_name, des1, NULL};
            execv("/bin/mv", argv);
          } // menunggu proses selesai //
          while ((wait(&stat)) > 0)
            ;
        }
        if (strstr(dp -> d_name, "air") != NULL) /*pengecekan apa file yg dipindahkan adalah
        file yang mengandung kata air*/
        {
          if ((child_id = fork()) == 0)
          {
            char *argv[] = {"mv", dp->d_name, des2, NULL};
            execv("/bin/mv", argv);
          }
          while ((wait(&stat)) > 0)
            ;
        }
        if (strstr(dp -> d_name, "amphibi") != NULL) /*pengecekan file yg dipindajlan adalah
        file yang mengandung kata amphibi*/
        {
          if ((child_id = fork()) == 0)
          {
            char *argv[] = {"mv", dp -> d _name, des3, NULL};
            execv("/bin/mv", argv);
          }
          while ((wait(&stat)) > 0)
            ;
        }
      }
    }
    closedir(folder);
  }
  waitpid(child_id, &stat, 0);
}
```
#### Penjelasan
Function `HewanPindah` bertujuan untuk memindahkan file berdasarkan kategori hewan (darat, air, dan amphibi) dari sebuah direktori ke direktori lainnya. Berikut adalah penjelasan dari setiap bagian kode:
``` c
void HewanPindah(char *soruce, char *des1, char *des2, char *des3)
``` 
Fungsi HewanPindah menerima empat argumen bertipe data char, yaitu soruce yang merupakan path dari direktori yang berisi file-file yang akan dipindahkan, des1 yang merupakan path dari direktori tujuan untuk file-file kategori hewan darat, des2 yang merupakan path dari direktori tujuan untuk file-file kategori hewan air, dan des3 yang merupakan path dari direktori tujuan untuk file-file kategori hewan amphibi.
``` c
int stat;
id_t child_id;
``` 
Mendeklarasikan variabel stat bertipe int dan child_id bertipe id_t yang akan digunakan untuk menampung status dari proses dan id dari child process.
``` c
struct dirent dp;
DIR folder;
folder = opendir(soruce);
```
Mendeklarasikan variabel dp bertipe struct dirent dan folder bertipe DIR. dp digunakan untuk menampung nama file dan tipe file yang sedang diproses sedangkan folder digunakan untuk membuka direktori yang akan dipindahkan.
``` c
if (folder != NULL)
{
  while ((dp = readdir(folder)) != NULL)
  {
    if (dp -> d_type == DT_REG)
    {
      // ...
    }
  }
  closedir(folder);
}
```
Pengecekan apakah direktori yang akan dipindahkan berhasil dibuka atau tidak. Jika berhasil dibuka maka seluruh file yang berada di dalam direktori tersebut akan diproses satu per satu.
``` c
if (dp -> d_type == DT_REG)
{
  if (strstr(dp -> d_name, "darat") != NULL)
  {
    // ...
  }
  if (strstr(dp -> d_name, "air") != NULL)
  {
    // ...
  }
  if (strstr(dp -> d_name, "amphibi") != NULL)
  {
    // ...
  }
}
```
Pengecekan apakah file yang sedang diproses merupakan file reguler atau bukan. Hanya file reguler saja yang akan diproses dan dipindahkan.
```c
if ((child_id = fork()) == 0)
{
  char *argv[] = {"mv", dp -> d_name, des1, NULL};
  execv("/bin/mv", argv);
}
while ((wait(&stat)) > 0)
  ;
``` 
Jika file yang sedang diproses merupakan file kategori hewan darat maka program akan membuat proses baru untuk menjalankan perintah mv dengan argumen yang sesuai. Proses ini dijalankan dengan menggunakan fungsi fork(). Setelah proses child selesai, parent process akan menunggu dengan menggunakan perintah wait().
``` c
waitpid(child_id, &stat, 0);
```
Menunggu hingga semua child process selesai dijalankan dengan menggunakan perintah waitpid(). Hal ini dilakukan untuk memastikan bahwa proses pemindahan file selesai dilakukan sebelum

### `zipper()`
``` c
void zipper()
{
  id_t child_id;
  int stat;
  if ((child_id = fork()) == 0) /* pengecekan apakah variabel bernilai 0 atau tidak, jika
  0 berarti proses child*/
  {
    char *argv[] = {"zip", "-r", "HewanDarat.zip", "HewanDarat", NULL};
    /* untuk mendeklarasikan variabel argv yang bertipe data char dan init variabel tsb
    dengan argumen yang akan diguakan untuk menjalankan program zip*/
    execv("/usr/bin/zip", argv);
  }
  while ((wait(&stat)) > 0)
    ;
  if ((child_id = fork()) == 0)
  {
    char *argv[] = {"zip", "-r", "HewanAir.zip", "HewanAir", NULL};
    execv("/usr/bin/zip", argv);
  }
  while ((wait(&stat)) > 0)
    ;
  if ((child_id = fork()) == 0)
  {
    char *argv[] = {"zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL};
    execv("/usr/bin/zip", argv);
  }
  while ((wait(&stat)) > 0) //menunggu proses child selesai//
    ;
  waitpid(child_id, &stat, 0); //menunggu proses child selesai//
}
```

#### Penjelasan
Fungsi `zipper` yang berfungsi untuk melakukan kompresi pada tiga folder berbeda yang bernama "HewanDarat", "HewanAir", dan "HewanAmphibi". Fungsi ini menggunakan sistem operasi Unix/Linux untuk menjalankan perintah zip.

1. Pertama-tama membuat proses child menggunakan perintah fork(). 
2. Jika proses tersebut adalah proses child (nilai yang dikembalikan oleh fork() adalah 0), maka fungsi execv() akan dijalankan. 
3. Fungsi ini akan mengeksekusi program zip di direktori /usr/bin/zip dengan argumen yang didefinisikan dalam variabel argv.
4. Setelah eksekusi program zip selesai, proses child akan berhenti. Sedangkan proses parent akan menunggu proses child selesai menggunakan perintah wait(). 
5. Proses ini dilakukan untuk semua folder yang ingin dikompresi. 
6. Setelah ketiga folder selesai dikompresi, fungsi akan menunggu proses child terakhir selesai menggunakan perintah waitpid().










# Soal 2
# Soal 3
Pada program ini terdapat beberapa fungsi yaitu `download_file()`, `extract_zip()`, `delete_non_manutd_players()`, `categorize_players()`, dan `buatTim()`
### `download_file()`
``` c
void download_file(char *url, char *filename) {
    pid_t pid = fork();
    if (pid == 0) {
        execlp("curl", "curl", "-L", "-o", filename, url, NULL);
        exit(EXIT_FAILURE);
    } else if (pid > 0) {
        wait(NULL);
    } else {
        perror("fork() error");
        exit(EXIT_FAILURE);
    }
}
```
### `extract_zip()`
``` c
void extract_zip(char *filename) {
    pid_t pid = fork();
    if (pid == 0) {
        execlp("unzip", "unzip", filename, NULL);
        exit(EXIT_FAILURE);
    } else if (pid > 0) {
        wait(NULL);
        // menghapus file zip setelah diekstrak
        remove(filename);
    } else {
        perror("fork() error");
        exit(EXIT_FAILURE);
    }
}
```
### `delete_non_manutd_players()`
``` c
void delete_non_manutd_players() {
    pid_t pid;

    pid = fork();
    if (pid == 0) {
        char players_dir[256];
        snprintf(players_dir, sizeof(players_dir), "%s/players", getcwd(NULL, 0));

        if (chdir(players_dir) == -1) {
            perror("chdir() error");
            exit(EXIT_FAILURE);
        }

        DIR *dir;
        struct dirent *ent;
        if ((dir = opendir(".")) != NULL) {
            while ((ent = readdir(dir)) != NULL) {
                if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0) {
                    continue;
                }

                char filename[256];
                snprintf(filename, sizeof(filename), "%s", ent->d_name);

                char name[256];
                char club[256];
                char position[256];
                char rating[256];

                if (sscanf(filename, "%[^_]_%[^_]_%[^_]_%[^.].png", name, club, position, rating) != 4) {
                    fprintf(stderr, "Invalid file format: %s\n", filename);
                    continue;
                }


                if (strcmp(club, "ManUtd") != 0) {
                    char path[256];
                    snprintf(path, sizeof(path), "./%s", filename);
                    remove(path);
                }
            }

            closedir(dir);
        } else {
            perror("opendir() error");
            exit(EXIT_FAILURE);
        }

        exit(EXIT_SUCCESS);
    } else if (pid > 0) {
        wait(NULL);
    } else {
        perror("fork() error");
        exit(EXIT_FAILURE);
    }
}
```
### `categorize_players()`
``` c
void categorize_players() {
    pid_t pid[4];
    char *positions[] = {"Kiper", "Bek", "Gelandang", "Penyerang"};

    for (int i = 0; i < 4; i++) {
        pid[i] = fork();
        if (pid[i] == 0) {
            char players_dir[256];
            snprintf(players_dir, sizeof(players_dir), "%s/players", getcwd(NULL, 0));

            if (chdir(players_dir) == -1) {
                perror("chdir() error");
                exit(EXIT_FAILURE);
            }

            DIR *dir;
            struct dirent *ent;
            if ((dir = opendir(".")) != NULL) {
                while ((ent = readdir(dir)) != NULL) {
                    if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0) {
                        continue;
                    }
                    char filename[256];
                    snprintf(filename, sizeof(filename), "%s", ent->d_name);

                    char name[256];
                    char club[256];
                    char pos[256];
                    int rating;
                    if (sscanf(filename, "%[^_]_%[^_]_%[^_]_%d.png", name, club, pos, &rating) != 4) {
                        fprintf(stderr, "Invalid file format: %s\n", filename);
                        continue;
                    }

                    int valid_position = 0;
                    for (int j = 0; j < 4; j++) {
                        if (strcmp(pos, positions[j]) == 0) {
                            valid_position = 1;
                            break;
                        }
                    }
                    if (!valid_position || strcmp(club, "ManUtd") != 0) {
                        continue;
                    }

                    Player player;
                    snprintf(player.name, sizeof(player.name), "%s", name);
                    snprintf(player.club, sizeof(player.club), "%s", club);
                    snprintf(player.pos, sizeof(player.pos), "%s", pos);
                    player.rating = rating;
                    players[num_players] = player;
                    num_players++;

                    char dir_path[256];
                    snprintf(dir_path, sizeof(dir_path), "../%s", pos);

                    pid_t pid_mkdir = fork();
                    if (pid_mkdir == 0) {
                        char *args[] = {"mkdir", "-p", dir_path, NULL};
                        execvp(args[0], args);
                        perror("execvp() error");
                        exit(EXIT_FAILURE);
                    } else if (pid_mkdir < 0) {
                        perror("fork() error");
                        exit(EXIT_FAILURE);
                    }

                    char source[256];
                    snprintf(source, sizeof(source), "./%s", filename);
                    char dest[256];
                    snprintf(dest, sizeof(dest), "../%s/%s", pos, filename);

                    pid_t pid_rename = fork();
                    if (pid_rename == 0) {
                        char *args[] = {"mv", source, dest, NULL};
                        execvp(args[0], args);
                        perror("execvp() error");
                        exit(EXIT_FAILURE);
                    } else if (pid_rename < 0) {
                        perror("fork() error");
                        exit(EXIT_FAILURE);
                    } else {
                        waitpid(pid_rename, NULL, 0); // menunggu proses rename file selesai
                    }

                }
                closedir(dir);
            } else {
                perror("opendir() error");
                exit(EXIT_FAILURE);
            }

            exit(EXIT_SUCCESS);
        } else if (pid[i] < 0) {
            perror("fork() error");
            exit(EXIT_FAILURE);
        }
    }

    for (int i = 0; i < 4; i++) {
        waitpid(pid[i], NULL, 0);
    }

    // remove players folder
    char players_dir[256];
    snprintf(players_dir, sizeof(players_dir), "%s/players", getcwd(NULL, 0));

    if (chdir("..") == -1) {
        perror("chdir() error");
        exit(EXIT_FAILURE);
    }

    if (rmdir(players_dir) == -1) {
        perror("rmdir() error");
        exit(EXIT_FAILURE);
    }
}
```
### `buatTim()`
``` c
int cmp_players(const void *p1, const void *p2) {

    Player *player1 = (Player *)p1;
    Player *player2 = (Player *)p2;

    return player2->rating - player1->rating;
}

void buatTim(int num_defenders, int num_midfielders, int num_strikers) {
    char filename[256];
    snprintf(filename, sizeof(filename), "/home/khairuddin/Formasi_%d-%d-%d.txt", num_defenders, num_midfielders, num_strikers); 

    qsort(players, num_players, sizeof(Player), cmp_players);

    FILE *fp = fopen(filename, "w");
    if (fp == NULL) {
        printf("Error opening file %s\n", filename);
        return;
    }

    fprintf(fp, "Formasi %d-%d-%d\n", num_defenders, num_midfielders, num_strikers);

    int num_goalkeepers = 1;
    int num_def = 0, num_mid = 0, num_str = 0;
    for (int i = 0; i < num_players && num_def < num_defenders && num_mid < num_midfielders && num_str < num_strikers; i++) {
        Player p = players[i];
        if (strcmp(p.pos, "Kiper") == 0 && num_goalkeepers > 0) {
            fprintf(fp, "Kiper\t%s\t%s\t%d\n", p.name, p.club, p.rating);
            num_goalkeepers--;
        } else if (strcmp(p.pos, "Bek") == 0 && num_def < num_defenders) {
            fprintf(fp, "Bek\t%s\t%s\t%d\n", p.name, p.club, p.rating);
            num_def++;
        } else if (strcmp(p.pos, "Gelandang") == 0 && num_mid < num_midfielders) {
            fprintf(fp, "Gelandang\t%s\t%s\t%d\n", p.name, p.club, p.rating);
            num_mid++;
        } else if (strcmp(p.pos, "Penyerang") == 0 && num_str < num_strikers) {
            fprintf(fp, "Penyerang\t%s\t%s\t%d\n", p.name, p.club, p.rating);
            num_str++;
        }
    }

    fclose(fp);
}
```
# Soal 4

```
int main(int argc, char **argv){
    if (argc-1 > 4 || argc-1 < 4){
        printf("Yahh salah input（>﹏<)\n");
    }else{
        if ( (strcmp(argv[1], "*") == 0) || (strcmp(argv[1], "0") >= 0 && strcmp(argv[1],"59") <= 0) ){
            if ( (strcmp(argv[2], "*") == 0) || (strcmp(argv[2], "0") >= 0 && strcmp(argv[2],"59") <= 0) ){
                if ( (strcmp(argv[3], "*") == 0) || (strcmp(argv[3], "0") >= 0 && strcmp(argv[3],"23") <= 0) ){
                    if (pattern_check(argv[4]) == 1){
                        service_maker(argv[1], argv[2], argv[3], argv[4]);
```


 - input melalui argumen pada terminal
 - pada c kita menambahkan int argc sebagai penampung banyaknya argumen pada terminal dan char **argv sebagai array dari setiap argumen
 - melakukan cek ke argumen ke 1 (detik) dengan kondisi input akan diterima ketika sama dengan "*" atau diantara 0 dan 59 dengan membandingkan (strcmp) input dengan karakter 0 dan 59. Jika tidak memenuhi salah satu dari kedua pengecekan maka akan output "Hmmm argumen 1 salah (｡•́︿•̀｡)".
 - dengan syarat pengecekan di atas berhasil, lakukan cek ke argumen ke 2 (menit) dengan kondisi input akan diterima ketika sama dengan "*" atau diantara 0 dan 59 dengan membandingkan (strcmp) input dengan karakter 0 dan 59. Jika tidak memenuhi salah satu dari kedua pengecekan maka akan output "Hmmm argumen 2 salah (｡•́︿•̀｡)".
 - Lakukan cek ke argumen ke 3 (bagian jam) dengan kondisi input akan diterima ketika sama dengan "*" atau diantara 0 dan 23 dengan membandingkan (strcmp) input dengan karakter 0 dan 59. Jika tidak memenuhi salah satu dari kedua pengecekan maka akan output "Hmmm argumen 3 salah (｡•́︿•̀｡)".
 - pengecekan terakhir dengan syarat pengecekan di atas berhasil, lakukan cek ke argumen ke 4 (path) dengan kondisi input akan dipassing pada fungsi :

```
int pattern_check(char * paths){
    FILE *file;
    if ((file = fopen(paths, "r")))
    {
        fclose(file);
        return 1;
    }
    return 0;
}
```

 - digunakan struct file bertipe FILE lalu file tersebut diinisiasi dengan fopen file dengan metode r. Ketika fopen sukses maka akan mengembalikan nilai 1 (true) sebaliknya ketika gagal akan mengembalikan nilai 0 (false)

```
 void service_maker(char * second, char * minutes, char * hour, char * path_file){
    // Variabel menyimpan PID
    pid_t pid, sid;

    // Menyimpan PID dari Child Process
    pid = fork();

// Saat fork gagal
if (pid < 0) {
exit(EXIT_FAILURE);
}

// Saat fork berhasil
if (pid > 0) {
exit(EXIT_SUCCESS);
}

umask(0);

sid = setsid();
if (sid < 0) {
exit(EXIT_FAILURE);
}

close(STDIN_FILENO);
close(STDOUT_FILENO);
close(STDERR_FILENO);
    while (1) {
        time_t times;
        struct tm* tm_info;  
        times = time(NULL);
        tm_info = localtime(&times);
        char skrg_jam[5];
        sprintf(skrg_jam,"%d", tm_info->tm_hour);
        char skrg_menit[5]; 
        sprintf(skrg_menit, "%d", tm_info->tm_min);
        char skrg_detik[5];
        sprintf(skrg_detik, "%d", tm_info->tm_sec);
        if( strcmp(hour, "*")==0)
        {
            if(strcmp(minutes,"*") == 0)
            {
                if(strcmp(second,"*")==0)
                {
                    char *args[] = {"bash", path_file, NULL};
                    subv("/bin/bash", args);
                }
                else if(strcmp(skrg_detik, second)==0)
                {
                    char *args[] = {"bash", path_file, NULL};
                    subv("/bin/bash", args);
                }
            }
            else if(strcmp(skrg_menit, minutes))
            {
                if(strcmp(second, "*")==0)
                {
                    char *args[] = {"bash", path_file, NULL};
                    subv("/bin/bash", args);
                }
                else if(strcmp(skrg_detik,second)==0)
                {
                    char *args[] = {"bash", path_file, NULL};
                    subv("/bin/bash", args);
                }
            }
        }
        else if( strcmp(hour,skrg_jam)==0)
        {
            if(strcmp(minutes,"*") == 0)
            {
                if(strcmp(second,"*")==0)
                {
                    char *args[] = {"bash", path_file, NULL};
                    subv("/bin/bash", args);
                }
                else if(strcmp(skrg_detik,path_file)==0)
                {
                    char *args[] = {"bash", path_file, NULL};
                    subv("/bin/bash", args);
                }
            }
            else if(strcmp(skrg_menit, minutes)==0)
            {
                if(strcmp(second,"*")==0)
                {
                    char *args[] = {"bash", path_file, NULL};
                    subv("/bin/bash", args);
                }
                else if(strcmp(skrg_detik,second)==0)
                {
                    char *args[] = {"bash", path_file, NULL};
                    subv("/bin/bash", args);
                }
             }
        }   
        sleep(1);
    }
}
```


 - Parameter yang digunakan pada fungsi ini yaitu string hour,minutes,second serta string path file bash yang akan dijalankan.

```
 while (1) {
        time_t times;
        struct tm* tm_info;  
        times = time(NULL);
        tm_info = localtime(&times);
        char skrg_jam[5];
        sprintf(skrg_jam,"%d", tm_info->tm_hour);
        char skrg_menit[5]; 
        sprintf(skrg_menit, "%d", tm_info->tm_min);
        char skrg_detik[5];
        sprintf(skrg_detik, "%d", tm_info->tm_sec);
        if( strcmp(hour, "*")==0)
```


 - menggunakan while karena program yang dibuat seperti crontab sehingga akan dilakukan terus menerus. 
 
 ```
char skrg_jam[5];
        sprintf(skrg_jam,"%d", tm_info->tm_hour);
        char skrg_menit[5]; 
        sprintf(skrg_menit, "%d", tm_info->tm_min);
        char skrg_detik[5];
        sprintf(skrg_detik, "%d", tm_info->tm_sec);
        if( strcmp(hour, "*")==0)
```


 - digunakan untuk mengubah masing-masing nilai integer jam,menit,detik pada struct *tm_info ke string skrg_jam, skrg_menit dan skrg_detik menggunakan bantuan fungsi sprintf

 ```
if( strcmp(hour, "*")==0)
        {
            if(strcmp(minutes,"*") == 0)
            {
                if(strcmp(second,"*")==0)
                {
                    char *args[] = {"bash", path_file, NULL};
                    subv("/bin/bash", args);
                }
                else if(strcmp(skrg_detik, second)==0)
                {
                    char *args[] = {"bash", path_file, NULL};
                    subv("/bin/bash", args);
                }
            }
            else if(strcmp(skrg_menit, minutes))
            {
                if(strcmp(second, "*")==0)
                {
                    char *args[] = {"bash", path_file, NULL};
                    subv("/bin/bash", args);
                }
                else if(strcmp(skrg_detik,second)==0)
                {
                    char *args[] = {"bash", path_file, NULL};
                    subv("/bin/bash", args);
                }
            }
        }
        else if( strcmp(hour,skrg_jam)==0)
        {
            if(strcmp(minutes,"*") == 0)
            {
                if(strcmp(second,"*")==0)
                {
                    char *args[] = {"bash", path_file, NULL};
                    subv("/bin/bash", args);
                }
                else if(strcmp(skrg_detik,path_file)==0)
                {
                    char *args[] = {"bash", path_file, NULL};
                    subv("/bin/bash", args);
                }
            }
            else if(strcmp(skrg_menit, minutes)==0)
            {
                if(strcmp(second,"*")==0)
                {
                    char *args[] = {"bash", path_file, NULL};
                    subv("/bin/bash", args);
                }
                else if(strcmp(skrg_detik,second)==0)
                {
                    char *args[] = {"bash", path_file, NULL};
                    subv("/bin/bash", args);
                }
```


 - mengecek apakah argumen hour yang dimasukkan merupakan * atau sama dengan jam waktu saat itu yang ada di string skrg_jam,
 - dilakukan lagi pengecekan apakah argumen minutes yang dimasukkan merupakan * atau sama dengan menit waktu saat itu yang ada di string skrg_menit.
 - dilakukan pengecekan lagi apakah argumen second yang dimasukkan sama dengan * atau sama dengan menit waktu saat itu yang ada di string skrg_detik.
 - ika memenuhi kondisi-kondisi tersebut akan dipanggil fungsi subv untuk mengeksekusi bash pada string path_file yang ada pada argumen terakhir inputan.
 - elanjutnya kode sleep(1); yang terletak di dalam while(1) mengisyaratkan bahwa fungsi tersebut setiap berjalan akan melakukan delay 1 detik. Sehingga setiap 1 detik fungsi ini akan melakukan penyocokan antara waktu di argumen dengan waktu saat itu

 ```
void subv(char *path, char * const argv[])
{
    int ret;
    pid_t a_pid = fork();
    if (a_pid == -1) return;
    if (a_pid != 0) 
    { 
        wait(&ret); 
        return; 
    }
    execv(path, argv);
}
```


 - fungsi subv dengan parameter string path dan argumen untuk digunakan nanti pada execv.
 - Secara default fungsi ini akan menjalankan execv dengan parameter string path serta argumen yang sudah dipassing di fungsi ini saat dipanggil
 
