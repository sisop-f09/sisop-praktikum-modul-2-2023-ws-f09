#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <string.h>
#include <pwd.h>
#include <regex.h>
#include <sys/stat.h>


// nama nama fungsi//
void downloadFile(char *url, char *namefile);
void unzipper(char *sourceDir);
void getFile(char *directory);
void newDirectory();
void HewanPindah(char *soruce, char *des1, char *des2, char *des3);
void zipper();


int main() {
  /* untuk mendownload file yang diinginkan, maka akan kita panggil fungsi download*/

  downloadFile("https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "binatang.zip");

  /* jika sudah selesai di download maka akan dialankan perintah unzip untuk melakukan unzip*/

  unzipper("binatang.zip");
  getFile(".");

  /** jika sudah seelsai di unzip, maka akan memanggil fungsi generate directory dengan nama
   * HewanDarat, HewanAir, dan HewanAmphibi, dan apabila selesai membuat directory, grape 
   * menjalankan filter atau pemindahan filter hewan sesuai dengan tempat tinggalnya */
  newDirectory();
  HewanPindah(".", "HewanDarat", "HewanAir", "HewanAmphibi");

  /** jika sudah selesai mengetahui hewan yang harus dijaga, maka akan dijalankan zip
   * dengan fungsi zipper
   */
  zipper();
}

void downloadFile(char *url, char *namefile)
{
  int stat; /* untuk mendeklarasikan variabel stat yang bertipe data int*/
  pid_t IDdownload = fork(); /* untuk mendeklarasikan variabel stat yang bertipe data var*/

  if (IDdownload == 0) //melakukan  kondisi pengececekan, apabila 0 berarti proses child//
  {
    char *args[] = {"wget", "--no-check-certificate", url, "-q", "-O", namefile, NULL};
    execv("/usr/bin/wget", args); //untuk menjalankan program wget dengan argumen ags//
  }

  // fungsi untuk menunggu proses child selesai lanjut ke proses parent
  waitpid(IDdownload, &stat, 0); 
}

void unzipper(char *sourceDir)
{
  pid_t IDunzip = fork(); /** mendeklarasikan var unzipId bertipe data pid_t 
  dengan init variabel tersebut dengan fungsi fork*/

  if (IDunzip == 0) // melakukan kondisi pengecekan, apabila 0 maka proses child*/
  {
    char *args[] = {"unzip", "-q", sourceDir, "-d", ".", NULL};
    execv("/usr/bin/unzip", args);
  }

  waitpid(IDunzip, NULL, 0); //untuk menunggu proses child selesai//
}

void getFile(char *directory)
{
  int stat;
  char *path[100];

  struct dirent *dp;
  DIR *folder;
  srand(time(NULL));
  folder = opendir(directory);

  if (folder != NULL)
  {
    int i = 0;
    while ((dp = readdir(folder)) != NULL)
    {
      if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".jpg") != NULL)
      {
        if (dp->d_type == DT_REG)
        {
          char *token = dp->d_name;
          path[i] = token;
          i++;
        }
      }
    }
    int size = sizeof(*path);
    int random = rand() % size;

    // Create file txt
    printf("Hewan Yang Dijaga : %s \n", strtok(path[random], "."));
    FILE *file;
    file = fopen("penjaga.txt", "w");
    fprintf(file, "Hewan Yang Dijaga : %s", strtok(path[random], "."));
    fclose(file);
    closedir(folder);
  }
}

void newDirectory()
{
  int stat;
  id_t child_id;

  if ((child_id = fork()) == 0) /* melakukan proses pengecekan, apabila 0 maka proses child*/
  {
    char *args[] = {"mkdir", "-p", "HewanDarat", NULL};
    execv("/bin/mkdir", args); 
  }
  while ((wait(&stat)) > 0) /* menunggu proses child selesai*/
    ;

  if ((child_id = fork()) == 0)
  {
    char *args[] = {"mkdir", "-p", "HewanAir", NULL};
    execv("/bin/mkdir", args); /* menjalankan program mkdir dengan argumen args 
    sebanyak 3 kali*/
  }
  while ((wait(&stat)) > 0)
    ;
  if ((child_id = fork()) == 0)
  {
    char *args[] = {"mkdir", "-p", "HewanAmphibi", NULL};
    execv("/bin/mkdir", args);
  }
  while ((wait(&stat)) > 0)
    ;

  waitpid(child_id, &stat, 0); /* untuk menunggu proses child selesai*/
}

void HewanPindah(char *soruce, char *des1, char *des2, char *des3)
{
  int stat;
  id_t child_id;
  struct dirent dp; /*untuk mendeklarasikan variabel dp dgn tipe data struct dirent*/
  DIR folder; /*mendeklarasi variabel folder yang bertipe data DIR*/
  folder = opendir(soruce); /* membuka direktori yang dipindahkan*/

  if (folder != NULL) //melakukan pengecekan apakah ada atau tidak//
  {
    while ((dp = readdir(folder)) != NULL) /*perulangan selama direk yang dipindahkan 
    masih ada*/
    {
      if (dp -> d_type == DT_REG) /*melakukan pengecekan apakah file yang dipindahkan 
      file biasa atau bukan*/
      {
        // jika cocok dengan darat
        if (strstr(dp -> d_name, "darat") != NULL)
        { //pengecekan apakah 0 atau tidak, jika 0 maka proses child//
          if ((child_id = fork()) == 0)
          { // deklarasi var argv tipe data char dengan init argumen yang digunakan untuk 
          // menjalankan program mv//
            char *argv[] = {"mv", dp -> d_name, des1, NULL};
            execv("/bin/mv", argv);
          } // menunggu proses selesai //
          while ((wait(&stat)) > 0)
            ;
        }
        if (strstr(dp -> d_name, "air") != NULL) /*pengecekan apa file yg dipindahkan adalah
        file yang mengandung kata air*/
        {
          if ((child_id = fork()) == 0)
          {
            char *argv[] = {"mv", dp->d_name, des2, NULL};
            execv("/bin/mv", argv);
          }
          while ((wait(&stat)) > 0)
            ;
        }
        if (strstr(dp -> d_name, "amphibi") != NULL) /*pengecekan file yg dipindajlan adalah
        file yang mengandung kata amphibi*/
        {
          if ((child_id = fork()) == 0)
          {
            char *argv[] = {"mv", dp -> d _name, des3, NULL};
            execv("/bin/mv", argv);
          }
          while ((wait(&stat)) > 0)
            ;
        }
      }
    }
    closedir(folder);
  }
  waitpid(child_id, &stat, 0);
}

void zipper()
{
  id_t child_id;
  int stat;
  if ((child_id = fork()) == 0) /* pengecekan apakah variabel bernilai 0 atau tidak, jika
  0 berarti proses child*/
  {
    char *argv[] = {"zip", "-r", "HewanDarat.zip", "HewanDarat", NULL};
    /* untuk mendeklarasikan variabel argv yang bertipe data char dan init variabel tsb
    dengan argumen yang akan diguakan untuk menjalankan program zip*/
    execv("/usr/bin/zip", argv);
  }
  while ((wait(&stat)) > 0)
    ;
  if ((child_id = fork()) == 0)
  {
    char *argv[] = {"zip", "-r", "HewanAir.zip", "HewanAir", NULL};
    execv("/usr/bin/zip", argv);
  }
  while ((wait(&stat)) > 0)
    ;
  if ((child_id = fork()) == 0)
  {
    char *argv[] = {"zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL};
    execv("/usr/bin/zip", argv);
  }
  while ((wait(&stat)) > 0) //menunggu proses child selesai//
    ;
  waitpid(child_id, &stat, 0); //menunggu proses child selesai//
}