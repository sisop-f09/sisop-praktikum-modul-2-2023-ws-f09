#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <wait.h>
#include <dirent.h>
#include <time.h>

void newFolder(char folderName[]){
	pid_t PID;
	PID = fork();
	
	int status;
	if(PID == 0){
		char *argv[] = {"mkdir", "-p", folderName, NULL};
		execv("/bin/mkdir", argv);
	}
	
	while ((wait(&status)) > 0);
}

void downloadPicture(char folderName[]){
	pid_t PID;
	int status;
	
	for(int i = 0; i < 15; i++){
		PID = fork();
		time_t t = time(NULL);
		struct tm currTime = *localtime(&t);
		char fileName[100];
		clock_t start = time(NULL), end;
		strftime(fileName, sizeof(fileName), "%Y-%m-%d_%H:%M:%S", &currTime);
		
		if(PID == 0){
			char picSource[100] = "https://picsum.photos/";
			char picSize[25];
			sprintf(picSize, "%ld", t%1000+50);
			strcat(picSource, picSize);
			
			
			char saveFile[300];
			sprintf(saveFile, "/home/abyan/Documents/Sisop/Praktikum2/soal3/%s/%s.jpeg", folderName, fileName);
			
			char *argv[] = {"wget", "-q", picSource, "-O", saveFile, NULL};
			execv("/bin/wget", argv);
		}
		
		sleep(5);
	}
}

void ziping(char folderName[]){
	pid_t PID;
	PID = fork();
	int status;
	
	if(PID == 0){
		char zipFile[100];
		sprintf(zipFile, "%s.zip", folderName);
		char *argv[] = {"zip", "-qr", zipFile, folderName, NULL};
		execv("/bin/zip", argv);
	}
	
	while(wait(&status) > 0);
}


void killer(const char *argv[], int sid){
	FILE *fptr;
	fptr = fopen("killer.sh", "w");
	fputs("#!/bin/bash\n\n", fptr);
	char killerLine[100];
	
	if(!strcmp(argv[1], "-a")){
		sprintf(killerLine, "killall -9 %s\nrm killer.sh", argv[0]);
		fputs(killerLine, fptr);
	}else if(!strcmp(argv[1], "-b")){
		sprint(killerLine, "kill -9\nrm killer.sh", sid);
		fputs(killerLine, fptr);
	}
	
	fclose(fptr);
}

int main(inr argc, const char **argv){
	pid_t parentPID;
	parentPID = getpid();
	
	if(!strcmp(argv[1], "-a") || !strcmp(argv[1], "-b")){
		killer(argv, (int)parentPID);
		
		while(1){
			time_t t =  itme(NULL);
			struct tm currTime = *localtime(&t);
			char folderName[100];
			clock_t start = time(NULL), end;
			
			if(fork() == 0){
				strftime(folderName, sizeof(folderName), "%Y-%m-%d_%H:%M:%S", &currTime);
				newFolder(folderName);
				downloadPicture(folderName);
				ziping(folderName);
				exit(0);
			}
			
			sleep(40);
		}
	}
}





