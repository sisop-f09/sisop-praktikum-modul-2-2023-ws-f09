#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/wait.h>

#define MAX_PLAYERS 100

void download_file(char *url, char *filename);
void extract_zip(char *filename);
void delete_non_manutd_players();
void categorize_players();
int cmp_players(const void *p1, const void *p2);
void buatTim(int num_defenders, int num_midfielders, int num_strikers);

typedef struct {
    char name[256];
    char club[256];
    char pos[256];
    int rating;
} Player;
Player players[MAX_PLAYERS];
int num_players = 0;

int main() {
    char url[] = "https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF";
    char filename[] = "players.zip";

    // Mengunduh file zip
    download_file(url, filename);

    // mengekstrak file zip
    extract_zip(filename);

    // menghapus pemain yang bukan dari ManUtd
    delete_non_manutd_players();

    // mengkategorikan pemain berdasarkan posisinya
    categorize_players();

    // membuat file tim terbaik
    buatTim(4, 4, 2);

    return 0;
}

void download_file(char *url, char *filename) {
    pid_t pid = fork();
    if (pid == 0) {
        execlp("curl", "curl", "-L", "-o", filename, url, NULL);
        exit(EXIT_FAILURE);
    } else if (pid > 0) {
        wait(NULL);
    } else {
        perror("fork() error");
        exit(EXIT_FAILURE);
    }
}

void extract_zip(char *filename) {
    pid_t pid = fork();
    if (pid == 0) {
        execlp("unzip", "unzip", filename, NULL);
        exit(EXIT_FAILURE);
    } else if (pid > 0) {
        wait(NULL);
        // menghapus file zip setelah diekstrak
        remove(filename);
    } else {
        perror("fork() error");
        exit(EXIT_FAILURE);
    }
}


void delete_non_manutd_players() {
    pid_t pid;

    pid = fork();
    if (pid == 0) {
        char players_dir[256];
        snprintf(players_dir, sizeof(players_dir), "%s/players", getcwd(NULL, 0));

        if (chdir(players_dir) == -1) {
            perror("chdir() error");
            exit(EXIT_FAILURE);
        }

        DIR *dir;
        struct dirent *ent;
        if ((dir = opendir(".")) != NULL) {
            while ((ent = readdir(dir)) != NULL) {
                if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0) {
                    continue;
                }

                char filename[256];
                snprintf(filename, sizeof(filename), "%s", ent->d_name);

                char name[256];
                char club[256];
                char position[256];
                char rating[256];

                if (sscanf(filename, "%[^_]_%[^_]_%[^_]_%[^.].png", name, club, position, rating) != 4) {
                    fprintf(stderr, "Invalid file format: %s\n", filename);
                    continue;
                }


                if (strcmp(club, "ManUtd") != 0) {
                    char path[256];
                    snprintf(path, sizeof(path), "./%s", filename);
                    remove(path);
                }
            }

            closedir(dir);
        } else {
            perror("opendir() error");
            exit(EXIT_FAILURE);
        }

        exit(EXIT_SUCCESS);
    } else if (pid > 0) {
        wait(NULL);
    } else {
        perror("fork() error");
        exit(EXIT_FAILURE);
    }
}


void categorize_players() {
    pid_t pid[4];
    char *positions[] = {"Kiper", "Bek", "Gelandang", "Penyerang"};

    for (int i = 0; i < 4; i++) {
        pid[i] = fork();
        if (pid[i] == 0) {
            char players_dir[256];
            snprintf(players_dir, sizeof(players_dir), "%s/players", getcwd(NULL, 0));

            if (chdir(players_dir) == -1) {
                perror("chdir() error");
                exit(EXIT_FAILURE);
            }

            DIR *dir;
            struct dirent *ent;
            if ((dir = opendir(".")) != NULL) {
                while ((ent = readdir(dir)) != NULL) {
                    if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0) {
                        continue;
                    }
                    char filename[256];
                    snprintf(filename, sizeof(filename), "%s", ent->d_name);

                    char name[256];
                    char club[256];
                    char pos[256];
                    int rating;
                    if (sscanf(filename, "%[^_]_%[^_]_%[^_]_%d.png", name, club, pos, &rating) != 4) {
                        fprintf(stderr, "Invalid file format: %s\n", filename);
                        continue;
                    }

                    int valid_position = 0;
                    for (int j = 0; j < 4; j++) {
                        if (strcmp(pos, positions[j]) == 0) {
                            valid_position = 1;
                            break;
                        }
                    }
                    if (!valid_position || strcmp(club, "ManUtd") != 0) {
                        continue;
                    }

                    Player player;
                    snprintf(player.name, sizeof(player.name), "%s", name);
                    snprintf(player.club, sizeof(player.club), "%s", club);
                    snprintf(player.pos, sizeof(player.pos), "%s", pos);
                    player.rating = rating;
                    players[num_players] = player;
                    num_players++;

                    char dir_path[256];
                    snprintf(dir_path, sizeof(dir_path), "../%s", pos);

                    pid_t pid_mkdir = fork();
                    if (pid_mkdir == 0) {
                        char *args[] = {"mkdir", "-p", dir_path, NULL};
                        execvp(args[0], args);
                        perror("execvp() error");
                        exit(EXIT_FAILURE);
                    } else if (pid_mkdir < 0) {
                        perror("fork() error");
                        exit(EXIT_FAILURE);
                    }

                    char source[256];
                    snprintf(source, sizeof(source), "./%s", filename);
                    char dest[256];
                    snprintf(dest, sizeof(dest), "../%s/%s", pos, filename);

                    pid_t pid_rename = fork();
                    if (pid_rename == 0) {
                        char *args[] = {"mv", source, dest, NULL};
                        execvp(args[0], args);
                        perror("execvp() error");
                        exit(EXIT_FAILURE);
                    } else if (pid_rename < 0) {
                        perror("fork() error");
                        exit(EXIT_FAILURE);
                    } else {
                        waitpid(pid_rename, NULL, 0); // menunggu proses rename file selesai
                    }

                }
                closedir(dir);
            } else {
                perror("opendir() error");
                exit(EXIT_FAILURE);
            }

            exit(EXIT_SUCCESS);
        } else if (pid[i] < 0) {
            perror("fork() error");
            exit(EXIT_FAILURE);
        }
    }

    for (int i = 0; i < 4; i++) {
        waitpid(pid[i], NULL, 0);
    }

    // remove players folder
    char players_dir[256];
    snprintf(players_dir, sizeof(players_dir), "%s/players", getcwd(NULL, 0));

    if (chdir("..") == -1) {
        perror("chdir() error");
        exit(EXIT_FAILURE);
    }

    if (rmdir(players_dir) == -1) {
        perror("rmdir() error");
        exit(EXIT_FAILURE);
    }
}

int cmp_players(const void *p1, const void *p2) {

    Player *player1 = (Player *)p1;
    Player *player2 = (Player *)p2;

    return player2->rating - player1->rating;
}

void buatTim(int num_defenders, int num_midfielders, int num_strikers) {
    char filename[256];
    snprintf(filename, sizeof(filename), "/home/khairuddin/Formasi_%d-%d-%d.txt", num_defenders, num_midfielders, num_strikers); 

    qsort(players, num_players, sizeof(Player), cmp_players);

    FILE *fp = fopen(filename, "w");
    if (fp == NULL) {
        printf("Error opening file %s\n", filename);
        return;
    }

    fprintf(fp, "Formasi %d-%d-%d\n", num_defenders, num_midfielders, num_strikers);

    int num_goalkeepers = 1;
    int num_def = 0, num_mid = 0, num_str = 0;
    for (int i = 0; i < num_players && num_def < num_defenders && num_mid < num_midfielders && num_str < num_strikers; i++) {
        Player p = players[i];
        if (strcmp(p.pos, "Kiper") == 0 && num_goalkeepers > 0) {
            fprintf(fp, "Kiper\t%s\t%s\t%d\n", p.name, p.club, p.rating);
            num_goalkeepers--;
        } else if (strcmp(p.pos, "Bek") == 0 && num_def < num_defenders) {
            fprintf(fp, "Bek\t%s\t%s\t%d\n", p.name, p.club, p.rating);
            num_def++;
        } else if (strcmp(p.pos, "Gelandang") == 0 && num_mid < num_midfielders) {
            fprintf(fp, "Gelandang\t%s\t%s\t%d\n", p.name, p.club, p.rating);
            num_mid++;
        } else if (strcmp(p.pos, "Penyerang") == 0 && num_str < num_strikers) {
            fprintf(fp, "Penyerang\t%s\t%s\t%d\n", p.name, p.club, p.rating);
            num_str++;
        }
    }

    fclose(fp);
}



